Release notes: version 5.31-70.0
================================

**13th September 2018**. We are pleased to announce the release of
InterProScan 5 (**version 5.31-70.0**).

This release of InterProScan 5 includes a data update (using InterPro
version **70.0** data).

What’s new
~~~~~~~~~~

Data update
^^^^^^^^^^^

-  Synchronized with `InterPro version
   70.0 <http://www.ebi.ac.uk/interpro/release_notes.html>`__.
-  The addition of 662 InterPro entries.
-  An update to SFLD (4) and MobiDB Lite (2.0).
-  Integration of 886 new methods from the CATH-Gene3D (5), CDD (43),
   HAMAP (1), PANTHER (689), Pfam (119), PIRSF (1), PRINTS (2), ProDom
   (3), SFLD (22) and SMART (1) databases.

New features
^^^^^^^^^^^^

-  InterProScan XML and JSON outputs now include location fragment
   information, to assist with the modelling of discontinuous domains.
-  Additional sequence feature information provided for MobiDB Lite.
-  Read more about output format changes for this release in
   :ref:`Change log for InterProScan JSON output format`.

Bug fixes
^^^^^^^^^

-  Replaced the "rpsblast" binary shipped with InterProScan with a
   repackaged version that will work on a larger number of systems out
   of the box (without the need to recompile).

Known issues
^^^^^^^^^^^^

Documented on the following Wiki page: :ref:`Known issues`.


Reporting issues
^^^^^^^^^^^^^^^^

You found a bug? Or do you want to give us your feedback? Please use
`EMBL EBI's support form <http://www.ebi.ac.uk/support/interproscan>`__.
